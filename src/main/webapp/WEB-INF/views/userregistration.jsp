<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>WebAppForm</title>
        <!-- Favicon-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700">
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="icon" type="image/x-icon" href="${pageContext.request.contextPath}/resource/assets/favicon.ico" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="${pageContext.request.contextPath}/resource/css/styles.css" rel="stylesheet" >
        <link href="${pageContext.request.contextPath}/resource/css/custom.css" rel="stylesheet" >
    </head>
    <body>
        <!-- Responsive navbar-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="#">WebAppForm</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li class="nav-item"><a class="nav-link active" aria-current="page" href="${pageContext.request.contextPath}/user-register">Home</a></li>

                    </ul>
                </div>
            </div>
        </nav>
        <!-- Page content-->
        <div class="container">
            <div class="text-center mt-5">
            <form action="<%= request.getContextPath() %>/user-register" method="post">
            
            	<div class="row">
            	
            	<div class="col-md-2"></div>
            	
            
	            <div class="card col-md-8" >
				  <div class="card-header">
				    <h3>User Registration Form</h3>
				  </div>
				  <div class="card-body">
				    <div class="form-group">
						<div class="row">
							<div class="col-md-6"><input type="text" class="form-control" name="firstName" placeholder="First Name" required="required"></div>
							<div class="col-md-6"><input type="text" class="form-control" name="lastName" placeholder="Last Name" required="required"></div>
						</div>        	
			        </div>
			        <div class="form-group">
			        	<input type="email" class="form-control" name="email" placeholder="example@gmail.com" required="required">
			        </div>
					<div class="form-group">
			            <textarea class="form-control" name="address" rows="4" placeholder="Input Address" required="required"></textarea>
			        </div>
					
					<div class="form-group">
			            <button type="submit" class="btn btn-success btn-lg btn-block">Register Now</button>
			        </div>
				  </div>
				</div>
            
            <div class="col-md-2"></div>
            
                </div>
               </form>
            </div>
        </div>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="${pageContext.request.contextPath}/resource/js/scripts.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    </body>
</html>