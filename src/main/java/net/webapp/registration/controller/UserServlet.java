package net.webapp.registration.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.webapp.registration.dao.UserDao;
import net.webapp.registration.model.User;

/**
 * Servlet implementation class UserServlet
 */
@WebServlet("/user-register")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private UserDao userDao = new UserDao();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    response.getWriter().append("Served at: ").append(request.getContextPath());
		RequestDispatcher dsipatcher = request.getRequestDispatcher("/WEB-INF/views/userregistration.jsp");
		dsipatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		
		User user = new User();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		user.setAddress(address);
		
		try {
			userDao.registerUser(user);
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}
		
		RequestDispatcher dsipatcher = request.getRequestDispatcher("/WEB-INF/views/redirectdetails.jsp");
		dsipatcher.forward(request, response);
	}

}
