# web-app-form



## Getting started



## Project Download

```
cd your_preferred_directory
git clone https://gitlab.com/XMasud/web-app-form.git

```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/XMasud/web-app-form/-/settings/integrations)


## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Name
The name of this project is **WebAppForm**

## Description

The main purpose of this project is to collect user information in order to register. For the time being user have to provide 

- First Name
- Last Name
- Email
- Address

After submiting their data, they can return back to main page and able to entry new user's data again. 

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals

Example uses

<div align="center">
    <img src="/screenshots/demo.gif"></img> 
</div>

## Technologies

- Java
- JSP
- Sarvlet
- JBDC
- MySQL
- Tomcat v9.0

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License

This is a testing-purpose project, so no license is taken from anywhere. Feel free to use it.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
